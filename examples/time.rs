// Required features: uptime

use libscu::software::time;

fn print_time(_time: &libscu::types::Time, append_days: bool) {
    if append_days && _time.days > 0 {
        println!("  Days: {}", _time.days);
    }
    println!("  Hours: {}", _time.hours % 24);
    println!("  Minutes: {}", _time.minutes);
    println!("  Seconds: {}", _time.seconds);
}

fn main() {
    if let Some(uptime) = time::fetch_uptime() {
        println!("Uptime:");
        print_time(&uptime, true);
    }

    #[cfg(target_os = "linux")]
    {
        if let Some(date) = time::fetch_current_date() {
            println!("Date (d/m/y): {}/{}/{}", date.day, date.month, date.year);
        }

        if let Some(tn) = time::fetch_time_now() {
            println!("Time now:");
            print_time(&tn, false);
        }

        if let Some(timezone) = time::fetch_timezone() {
            println!("Timezone: {timezone}");
        }
    }
}
