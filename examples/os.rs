// Required features: os

use libscu::software::os;

fn main() {
    if let Some(os_release) = os::fetch_name() {
        println!("Name: {}", os_release.name);
        println!("Pretty name: {}", os_release.pretty_name);
    } else {
        println!("Cannot fetch OS release.")
    }
}
