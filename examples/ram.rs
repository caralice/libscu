// Required features: ram

use libscu::hardware::ram;

fn main() {
    let memory_info = ram::fetch_info();

    println!("RAM Total: {}MiB", memory_info.total.mb);
    println!("RAM Used: {}MiB", memory_info.used.mb);

    if let Some(swap) = memory_info.swap {
        println!("SWAP Total: {}MiB", swap.total.mb);
        println!("SWAP Used: {}MiB", swap.used.mb);
    }
}
