// Required features: bootmode

use libscu::hardware::bootmode;

fn main() {
    println!("System booted via: {:?}", bootmode::fetch_mode());
}
