// Required features: terminal

use libscu::software::terminal;

fn main() {
    let fetch_version = true;

    if let Ok(terminal_info) = terminal::fetch_info(fetch_version) {
        println!("Terminal name: {}", terminal_info.name);

        if let Some(version) = terminal_info.version {
            println!("- Version: {}", version);
        }

        if let Some(size) = terminal_info.size {
            println!("- Size: {}x{}", size.width, size.height)
        }
    } else {
        eprintln!("Failed to fetch information about terminal");
    }
}
