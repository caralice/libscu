// Required features: drives

use libscu::hardware::drives;

fn main() {
    if let Some(all_drives) = drives::fetch_all() {
        for drive in all_drives {
            println!("Device: {}", drive.dev_path);
            println!("- Model: {}", drive.model);
            println!("- Size: {:.1}GiB", drive.size.gb);
            println!("- Technology: {:?}", drive.technology);
            println!("- Is removable: {}", drive.removable);
            if let Some(partitions) = drive.partitions {
                println!("- Partitions:");
                for partition in partitions {
                    println!("  Device: {}", partition.dev_path);
                    println!("  - Partition number: {}", partition.number);
                    println!("  - Size: {:.1}GiB", partition.size.gb);
                    println!("  - Is read-only: {}", partition.readonly);
                }
            }
        }
    } else {
        println!("Platform not supported.");
    }
}
