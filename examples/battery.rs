// Required features: battery

use libscu::hardware::battery;

fn main() {
    let batteries = battery::fetch_all();
    if batteries.is_empty() {
        println!("No batteries detected");
    } else {
        for bat in batteries {
            if let Some(manufacturer) = bat.manufacturer {
                println!("Manufacturer: {manufacturer}");
            };
            println!("model: {}", bat.model);
            println!("Capacity: {}%", bat.capacity);
            if let Some(status) = bat.status {
                println!("Status: {status}");
            };
            if let Some(technology) = bat.technology {
                println!("Technology: {technology}");
            };
        }
    }
}
