// Required features: hostname

use libscu::software::hostname;

fn main() {
    let raw = true;
    if let Some(hostname) = hostname::fetch(raw) {
        println!("Hostname: {hostname}");
    }
}
