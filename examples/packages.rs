// Required features: packages

use libscu::software::packages;

fn main() {
    let package_managers = packages::fetch_all_managers();
    if !package_managers.is_empty() {
        for package_manager in package_managers {
            println!("Package manager: {}", package_manager.name);
            println!("- Number of packages: {}", package_manager.number_of_packages);
        }
    } else {
        println!("No package managers found.");
    }
}
