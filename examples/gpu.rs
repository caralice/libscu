// Required features: gpu

use libscu::hardware::gpu;

fn main() {
    let raw_model = true;
    if let Ok(gpus) = gpu::fetch_all(raw_model) {
        if !gpus.is_empty() {
            for gpu in gpus {
                println!("Vendor & Model: {} {}", gpu.vendor, gpu.model);
                if let Some(temperature) = gpu.temperature {
                    println!("- Temperature: {}°C", temperature);
                }
                if let Some(driver) = gpu.driver {
                    println!("- Driver: {}", driver);
                }
            }
        } else {
            println!("No GPUs found.");
        }
    }
}
