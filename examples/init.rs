// Required features: init

use libscu::software::init;

fn main() {
    if let Ok(init) = init::fetch_info() {
        println!("Init system: {}", init.name);
        if let Some(number_of_services) = init.number_of_services {
            println!("- Number of services: {}", number_of_services)
        }
    }
}
