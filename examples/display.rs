// Required features: display

use libscu::hardware::display;

fn main() {
    if let Some(brightness) = display::fetch_brightness() {
        println!("Max brightness: {}", brightness.max);
        println!("Current brightness: {}", brightness.current);
        println!(
            "Brightness percentage: {}%",
            ((brightness.current as f32 / brightness.max as f32) * 100_f32) as u32
        )
    }
}
