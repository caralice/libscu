// Required features: device

use libscu::hardware::device;

fn main() {
    if let Some(device_model) = device::fetch_model(true) {
        println!("Device model: {device_model}");
    }
}
