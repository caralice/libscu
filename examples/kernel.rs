// Required features: kernel

use libscu::software::kernel;

fn main() {
    let version = kernel::fetch_version();
    match version {
        Some(version) => println!("Kernel version: {}", version),
        None => eprintln!("Failed to fetch kernel version"),
    }
}
