// Required features: cpu

use libscu::hardware::cpu;

fn main() {
    let raw_model = true;
    if let Ok(cpu_info) = cpu::fetch_info(raw_model) {
        println!("Vendor: {}", cpu_info.vendor);
        println!("Model: {}", cpu_info.model);
        println!("Cores: {}", cpu_info.cores);
        println!("Threads: {}", cpu_info.threads);
        println!("Max frequency: {}GHz", cpu_info.frequency.ghz);
        if let Some(temperature) = cpu_info.temperature {
            println!("Temperature: {}°C", temperature);
        }
    } else {
        println!("No CPU information provided");
    }
}
