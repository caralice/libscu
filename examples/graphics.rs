// Required features: graphics

use libscu::software::graphics;

fn main() {
    #[cfg(target_os = "linux")]
    if let Some(display_server) = graphics::fetch_display_server() {
        println!("Display server: {:?}", display_server);
    }
    if let Some(desktop_environment) = graphics::fetch_environment() {
        println!("Desktop environment: {}", desktop_environment);
    }

    let fetch_version = true;
    if let Some(window_manager) = graphics::fetch_window_manager(fetch_version) {
        println!("Window manager: {}", window_manager.name);
        if let Some(version) = window_manager.version {
            println!("- Version: {}", version);
        }
    }
}
