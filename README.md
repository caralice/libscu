<div align="center">
  
# LIBSCU
[Rust](https://www.rust-lang.org) crate for fetching hardware/software information on Unix-like OSs
I'M GOING TO COMPLETELY REWRITE IT

[![Crates.io](https://img.shields.io/crates/v/libscu)](https://crates.io/crates/libscu)
[![Docs.rs](https://img.shields.io/badge/docs-main-informational)](https://docs.rs/libscu)

# Features support status

| Feature  | Description        | Android | Linux | MacOS |
| -------- | ------------------ | ------- | ----- | ----- |
| battery  | Battery state      | ❌       | ✅     | ❌     |
| bootmode | UEFI or BIOS       | ❌       | ✅     | ✅     |
| cpu      | CPU information    | ❌       | ✅     | ✅     |
| device   | Device name        | ✅       | ✅     | ✅     |
| display  | Display brightness | ✅       | ✅     | ❌     |
| drives   | Disks installed    | ❌       | ✅     | ❌     |
| gpu      | GPU's information  | ❌       | ✅     | ❌     |
| graphics | WM/DE information  | ✅       | ✅     | ✅     |
| hostname | Host name (in OS)  | ✅       | ✅     | ✅     |
| init     | Init system        | ❌       | ✅     | ✅     |
| kernel   | Kernel version     | ✅       | ✅     | ✅     |
| os       | OS name            | ✅       | ✅     | ✅     |
| packages | Package managers   | ✅       | ✅     | ✅     |
| proc     | Processes          | ✅       | ✅     | ✅     |
| ram      | RAM and SWAP       | ✅       | ✅     | ✅     |
| shell    | Shell              | ✅       | ✅     | ✅     |
| terminal | Terminal           | ✅       | ✅     | ✅     |
| uptime   | System uptime      | ✅       | ✅     | ✅     |
| users    | Users              | ✅       | ✅     | ✅     |

</dev>