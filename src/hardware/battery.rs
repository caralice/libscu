//! Fetch information about installed batteries
//!
//! Feature `battery` must be enabled
//!
//! Supported platforms: Linux

#![cfg(target_os = "linux")]
#![cfg(feature = "battery")]

use crate::util::fs::{get_prop, list_dir_from_str};

use std::path::PathBuf;

/// Contains basic information about battery
#[derive(Clone, Debug, Default)]
pub struct BatteryInfo {
    pub manufacturer: Option<String>,
    pub model: String,
    pub technology: Option<String>,
    pub capacity: u16,
    pub status: Option<String>,
}

fn fix_value(technology: Option<String>) -> Option<String> {
    if !technology
        .clone()
        .unwrap_or("Unknown".to_string())
        .contains("Unknown")
    {
        technology
    } else {
        None
    }
}

fn collect_info(battery_info_path: &PathBuf) -> Option<BatteryInfo> {
    if battery_info_path.exists() {
        let result = BatteryInfo {
            manufacturer: get_prop(battery_info_path, "manufacturer"),
            model: get_prop(battery_info_path, "model_name").unwrap_or("".to_string()),
            technology: fix_value(get_prop(battery_info_path, "technology")),
            capacity: get_prop(battery_info_path, "capacity")
                .and_then(|capacity| capacity.parse().ok())
                .unwrap_or(101),
            status: get_prop(battery_info_path, "status"),
        };

        if result.model.is_empty() && result.capacity == 101 {
            None
        } else {
            Some(result)
        }
    } else {
        None
    }
}

/// Returns [`Vec`]<[`BatteryInfo`]>
pub fn fetch_all() -> Vec<BatteryInfo> {
    let mut result = Vec::<BatteryInfo>::new();

    if let Ok(power_supply) = list_dir_from_str("/sys/class/power_supply", true) {
        for dir in power_supply {
            if dir
                .file_name()
                .unwrap_or_default()
                .to_str()
                .unwrap_or_default()
                .starts_with("BAT")
            {
                if let Some(bat_info) = collect_info(&dir) {
                    result.push(bat_info);
                }
            }
        }
    }

    result
}
