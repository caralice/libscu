//! Fetch device name
//!
//! Feature `device` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(any(target_os = "android", target_os = "linux", target_os = "macos"))]
#![cfg(feature = "device")]

use crate::util;

fn beautify(model: &str) -> String {
    let mut result = String::from(model);

    // remove trash
    for trash in [
        "System Product Name",
        "System Version",
        "To Be Filled By O.E.M.",
        "To be filled by O.E.M.",
        "Default string",
        "Type1ProductConfigId",
    ] {
        result = result.replace(trash, "");
    }

    result = result
        .replace("ASUSTeK COMPUTER INC.", "ASUS") // make ASUS brandname shorter
        .replace("Hewlett-Packard", "HP"); // make HP brandname shorter

    util::string::remove_multiple_spaces(result.replace(['\0', '\n'], "").trim())
}

mod android;
mod linux;
mod macos;

/// Returns device name (model or board name, depends on your luck :D)
///
/// on MacOS returns device model correctly
///
/// * raw arg - if false then trash like "To be filled by O.E.M." or "Default string" will be removed from result
pub fn fetch_model(raw: bool) -> Option<String> {
    let mut result = String::new();

    #[cfg(target_os = "android")]
    android::fetch_model(raw, &mut result);
    #[cfg(target_os = "linux")]
    linux::fetch_model(&mut result);
    #[cfg(target_os = "macos")]
    macos::fetch_model(&mut result).ok()?;

    if result.is_empty() {
        return None;
    }

    if !raw {
        result = beautify(&result);
    }

    // remove repeating words
    result = util::string::uniqalize(&result);

    Some(result.trim().to_string())
}
