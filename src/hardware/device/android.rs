#![cfg(target_os = "android")]

use crate::util::platform::android;

pub fn fetch_model(raw: bool, model_buf: &mut String) {
    let (mut brand, mut model) = (String::new(), String::new());

    for brand_prop in [
        "ro.product.brand",
        "ro.product.odm.brand",
        "ro.product.vendor.brand",
    ] {
        if let Some(_brand) = android::getprop(brand_prop) {
            brand = _brand;
            break;
        }
    }

    if !raw {
        brand = match brand.as_str() {
            "google" => "Google",
            "HONOR" => "Honor",
            another => another,
        }
        .to_string()
    }

    for model_prop in [
        "ro.config.marketing_name",
        "ro.product.marketname",
        "ro.product.odm.marketname",
        "ro.product.vendor.marketname",
        "ro.product.model",
        "ro.product.odm.model",
        "ro.product.vendor.model",
    ] {
        if let Some(_model) = android::getprop(model_prop) {
            if !_model.is_empty() {
                model = _model.replace("HONOR ", "");
                break;
            }
        }
    }

    if model.contains(&brand) {
        model = model[brand.len()+1..].to_string();
    }

    *model_buf = format!("{brand} {model}").trim().to_string();
}
