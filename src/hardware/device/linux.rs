#![cfg(target_os = "linux")]

use std::{fs, path::Path};

pub fn fetch_model(model_buf: &mut String) {
    let (brand, model): (String, String);

    if Path::new("/sys/devices/virtual/dmi/id/board_vendor").exists()
        && Path::new("/sys/devices/virtual/dmi/id/product_name").exists()
        && Path::new("/sys/devices/virtual/dmi/id/product_version").exists()
    {
        brand = fs::read_to_string("/sys/devices/virtual/dmi/id/board_vendor")
            .unwrap()
            .trim()
            .to_string();
        model = fs::read_to_string("/sys/devices/virtual/dmi/id/product_name")
            .unwrap()
            .trim()
            .to_string();
        let version = fs::read_to_string("/sys/devices/virtual/dmi/id/product_version").unwrap();

        *model_buf = format!("{brand} {model} {version}").to_string();
    } else if Path::new("/sys/devices/virtual/dmi/id/board_vendor").exists()
        && Path::new("/sys/devices/virtual/dmi/id/board_name").exists()
    {
        brand = fs::read_to_string("/sys/devices/virtual/dmi/id/board_vendor").unwrap();
        model = fs::read_to_string("/sys/devices/virtual/dmi/id/board_name").unwrap();

        *model_buf = format!("{brand} {model}");
    } else if Path::new("/sys/devices/virtual/dmi/id/product_name").exists()
        && Path::new("/sys/devices/virtual/dmi/id/product_version").exists()
    {
        let name = fs::read_to_string("/sys/devices/virtual/dmi/id/product_name").unwrap();
        let version = fs::read_to_string("/sys/devices/virtual/dmi/id/product_version").unwrap();

        *model_buf = format!("{name} {version}");
    }
}
