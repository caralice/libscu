//! Fetch information about installed GPU
//!
//! Feature `gpu` must be enabled \
//! Feature `pci_ids` is also required for fetching correct GPU name, if not enabled you will get only PCI ID \
//!
//! Supported platforms: Linux

#![cfg(target_os = "linux")]
#![cfg(feature = "gpu")]

mod linux;

use crate::util;

/// Contains basic information about GPU
#[derive(Clone, Debug, Default)]
pub struct GPUInfo {
    pub vendor: String,
    pub model: String,
    pub driver: Option<String>,
    pub temperature: Option<f32>,
}

fn beautify_model(gpuinfo: &mut GPUInfo, raw_model: bool) {
    if !raw_model && gpuinfo.model.contains("[") && gpuinfo.model.contains("]") {
        gpuinfo.model = gpuinfo
            .model
            .split(']')
            .next()
            .unwrap_or(&gpuinfo.model)
            .split('[')
            .nth(1)
            .unwrap_or(&gpuinfo.model)
            .to_string();
    }

    gpuinfo.model = gpuinfo.model.replace(&gpuinfo.vendor, "");

    gpuinfo.model = util::string::remove_multiple_spaces(&gpuinfo.model);
}

/// Returns vector with [`GPUInfo`]
/// 
/// raw_model arg - return [`GPUInfo::model`] without cutting trash and other processing (as is)
pub fn fetch_all(raw_model: bool) -> std::io::Result<Vec<GPUInfo>> {
    let mut result = Vec::<GPUInfo>::new();

    #[cfg(target_os = "linux")]
    linux::fetch_all(&mut result, raw_model)?;

    Ok(result)
}
