#![cfg(target_os = "linux")]

use crate::{types, util};

use std::{fs, path::PathBuf};

use super::*;

const SYSFS_BLOCKS: &str = "/sys/block";

#[derive(Clone, Debug)]
struct UEVENT {
    pub dev_name: String,
    pub dev_type: String,
}

impl UEVENT {
    fn parse(path: PathBuf) -> Option<Self> {
        let mut uevent = UEVENT {
            dev_name: String::new(),
            dev_type: String::new(),
        };

        if let Ok(uevent_read) = fs::read_to_string(path) {
            for line in uevent_read.split("\n").filter(|l| !l.is_empty()) {
                let (var, val) = line.trim().split_once("=").unwrap_or(("", ""));
                match var {
                    "DEVNAME" => uevent.dev_name = val.to_string(),
                    "DEVTYPE" => uevent.dev_type = val.to_string(),
                    _ => {}
                }
            }
        }

        if uevent.dev_name.is_empty() || uevent.dev_type.is_empty() {
            None
        } else {
            Some(uevent)
        }
    }
}

impl Partition {
    fn is_partition_dir(path: &PathBuf) -> bool {
        path.join("size").is_file()
            && path.join("uevent").is_file()
            && path.join("partition").is_file()
    }
    pub fn fetch_all(dev_name: &str) -> Option<Vec<Partition>> {
        if let Ok(dir_list) = util::fs::list_dir(PathBuf::from("/sys/block").join(dev_name), true) {
            let mut result = Vec::<Partition>::new();
            for partition_dir in dir_list
                .into_iter()
                .filter(|entry| Self::is_partition_dir(&entry))
            {
                let mut partition = Partition {
                    dev_path: String::new(),
                    number: 0,
                    size: Memory::default(),
                    readonly: false,
                };

                if !UEVENT::parse(partition_dir.join("uevent")).is_some_and(|uevent| {
                    if !uevent.dev_name.is_empty() {
                        partition.dev_path = format!("/dev/{}", uevent.dev_name);
                        return true;
                    }
                    false
                }) || !util::fs::get_prop(&partition_dir, "size").is_some_and(|size| {
                    size.parse::<i64>().is_ok_and(|size| {
                        partition.size = Memory::from_blocks(size);
                        true
                    })
                }) || !util::fs::get_prop(&partition_dir, "ro").is_some_and(|ro| {
                    partition.readonly = ro.contains("1");
                    true
                }) || !util::fs::get_prop(&partition_dir, "partition").is_some_and(|part_n| {
                    part_n.parse::<u8>().is_ok_and(|part_n| {
                        partition.number = part_n;
                        true
                    })
                }) {
                    continue;
                }

                result.push(partition)
            }

            if !result.is_empty() {
                result.sort_by(|f, s| f.number.cmp(&s.number));
                return Some(result);
            }
        }
        None
    }
}

mod disk_data {
    use super::*;
    fn is_virtual_disk(fname: &str) -> bool {
        fname.is_empty()
            || fname.starts_with("dm")
            || fname.starts_with("loop")
            || fname.starts_with("sr")
            || fname.starts_with("zram")
            || fname.starts_with("ram")
            || fname.contains("boot")
            || fname.starts_with("mtdblock")
            || fname.starts_with("block")
    }
    pub fn is_correct_disk(uevent: &UEVENT) -> bool {
        !is_virtual_disk(uevent.dev_name.as_str()) && uevent.dev_type == "disk"
    }
    pub fn technology(dev: &PathBuf) -> DriveTechnology {
        if super::util::fs::get_prop(&dev, "queue/rotational")
            .unwrap_or("".to_string())
            .contains("1")
        {
            DriveTechnology::HDD
        } else {
            DriveTechnology::SSD
        }
    }
    pub fn fetch_metadata(device: &mut Drive, dev: &PathBuf) {
        device.technology = technology(&dev);
        device.removable = util::fs::get_prop(&dev, "removable").unwrap_or("".to_string()) == "1";

        for model_name_file in ["model", "name"] {
            if let Some(m) = util::fs::get_prop(&dev.join("device"), model_name_file) {
                device.model = m;
                break;
            }
        }

        device.size = types::Memory::from_blocks(
            util::fs::get_prop(&dev, "size")
                .unwrap_or("".to_string())
                .parse::<i64>()
                .unwrap_or(0),
        );
    }
}

pub fn fetch_all() -> Option<Vec<Drive>> {
    let mut result: Vec<Drive> = Vec::new();

    let sysblock = PathBuf::from(SYSFS_BLOCKS);
    if !sysblock.is_dir() {
        return None;
    }
    for block_device in fs::read_dir(sysblock).ok()? {
        let mut device_buf = Drive::new();
        let block_device = block_device.unwrap();
        let dev = block_device.path();

        if let Some(uevent) = UEVENT::parse(dev.join("uevent")) {
            if !disk_data::is_correct_disk(&uevent) {
                continue;
            }

            device_buf.model = uevent.dev_name.clone();

            disk_data::fetch_metadata(&mut device_buf, &dev);
            if device_buf.size.mb == 0 {
                continue;
            }

            device_buf.dev_path = format!("/dev/{}", uevent.dev_name);
            device_buf.partitions = Partition::fetch_all(&uevent.dev_name);

            result.push(device_buf);
        }
    }

    Some(result)
}
