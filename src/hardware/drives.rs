//! Fetch information about installed drives
//!
//! Feature `drives` must be enabled
//!
//! Supported platforms: Linux

#![cfg(target_os = "linux")]
#![cfg(feature = "drives")]
#![allow(unreachable_code)]

use crate::types::Memory;

/// Contains SSD and HDD (don't trust that thing, because usb sticks is detected as HDD)
#[derive(Clone, Debug, Default, PartialEq)]
pub enum DriveTechnology {
    #[default]
    HDD,
    SSD,
}

/// Contains basic information about drive
#[derive(Clone, Debug, Default)]
pub struct Drive {
    pub dev_path: String,
    pub model: String,
    pub size: Memory,
    pub partitions: Option<Vec<Partition>>,
    pub technology: DriveTechnology,
    pub removable: bool,
}

impl Drive {
    pub fn new() -> Self {
        Self {
            dev_path: String::new(),
            model: String::new(),
            size: Memory::default(),
            partitions: None,
            technology: DriveTechnology::HDD,
            removable: false,
        }
    }
    /// I don't know why this function is here, it's useless i think
    pub fn is_ssd(&self) -> bool {
        self.technology == DriveTechnology::SSD
    }
}

/// Contains basic information about partition on drive
#[derive(Clone, Debug)]
pub struct Partition {
    pub dev_path: String,
    /// Partition number
    pub number: u8,
    pub size: Memory,
    pub readonly: bool,
}

mod linux;

/// Returns [`Option`]<[`Vec`]<[`Drive`]>> \
/// Uses a platform-specific function \
/// If platform isn't supported it will return [`None`] anyway
pub fn fetch_all() -> Option<Vec<Drive>> {
    #[cfg(target_os = "linux")]
    return linux::fetch_all();

    return None;
}
