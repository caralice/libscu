#![cfg(target_os = "linux")]

use super::BootMode;

/// Returns [`BootMode`]
///
/// Returns [`BootMode::UEFI`] if file /sys/firmware/efi/fw_platform_size exists, else [`BootMode::BIOS`]
pub fn fetch_mode() -> BootMode {
    match std::fs::metadata("/sys/firmware/efi/fw_platform_size") {
        Ok(_) => BootMode::UEFI,
        Err(_) => BootMode::BIOS,
    }
}