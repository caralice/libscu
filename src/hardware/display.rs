//! Fetch brightness level on display
//!
//! Feature `display` must be enabled
//!
//! Supported platforms: Android, Linux

#![cfg(any(target_os = "android", target_os = "linux"))]
#![cfg(feature = "display")]

/// Contains max and current brightness
#[derive(Clone, Debug, Default)]
pub struct Brightness {
    pub max: u32,
    pub current: u32,
}

mod android;
mod linux;

/// Returns Brightness structure \
/// returns None if platform is unsupported or brightness can't be fetched
pub fn fetch_brightness() -> Option<Brightness> {
    #[cfg(target_os = "android")]
    return android::brightness();
    #[cfg(target_os = "linux")]
    return linux::brightness();
}
