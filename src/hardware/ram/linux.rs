#![cfg(any(target_os = "linux", target_os = "android"))]

use crate::types;

use super::{RAMInfo, SWAPInfo};

use regex_lite::Regex;
use std::fs;

fn extract_i64(_str: &str) -> i64 {
    let re = Regex::new(r"\d+").unwrap();
    re.find(_str).unwrap().as_str().parse::<i64>().unwrap()
}

/// Returns [`RAMInfo`]
pub fn fetch_info() -> RAMInfo {
    // RAM
    let mut total: i64 = 0;
    let mut used: i64 = 0;

    // SWAP
    let mut swap_total: i64 = 0;
    let mut swap_free: i64 = 0;

    for line in fs::read_to_string("/proc/meminfo")
        .expect("NO /proc/meminfo FILE")
        .split('\n')
    {
        let mut line = line.split(':');
        let line_var = line.next().unwrap().trim();
        let line_val = line.next().unwrap_or("");
        if line_val.is_empty() {
            continue;
        }

        match line_var {
            "MemTotal" => {
                total = extract_i64(line_val);
                used = total;
            }
            "Shmem" => {
                used += extract_i64(line_val);
            }
            "MemFree" | "Buffers" | "Cached" | "SReclaimable" => {
                used -= extract_i64(line_val);
            }
            "SwapTotal" => {
                swap_total = extract_i64(line_val);
            }
            "SwapFree" => {
                swap_free = extract_i64(line_val);
            }
            _ => {
                continue;
            }
        }
    }

    RAMInfo {
        total: types::Memory::from_kb(total),
        used: types::Memory::from_kb(used),
        swap: if swap_total != 0 {
            Some(SWAPInfo {
                total: types::Memory::from_kb(swap_total),
                used: types::Memory::from_kb(swap_total - swap_free),
            })
        } else {
            None
        },
    }
}
