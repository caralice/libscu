//! Fetch which boot mode is used for booting OS
//!
//! Feature `bootmode` must be enabled
//!
//! Supported platforms: Linux, MacOS

#![cfg(any(target_os = "linux", target_os = "macos"))]
#![cfg(feature = "bootmode")]

mod linux;
mod macos;

/// Contains BIOS and UEFI entries
#[derive(Clone, Debug, Default, PartialEq)]
pub enum BootMode {
    /// Get more information on <https://en.wikipedia.org/wiki/BIOS>
    #[default]
    BIOS,
    /// Get more information on <https://en.wikipedia.org/wiki/UEFI>
    UEFI,
}

/// Returns [`BootMode`]
pub fn fetch_mode() -> BootMode {
    #[cfg(target_os = "linux")]
    return linux::fetch_mode();
    #[cfg(target_os = "macos")]
    return macos::fetch_mode();
}
