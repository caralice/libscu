#![cfg(target_os = "android")]

use super::Brightness;
use crate::util::platform::android;

pub fn brightness() -> Option<Brightness> {
    if let Some(prop) = android::getprop("debug.tracing.screen_brightness") {
        if let Ok(parsed_prop) = prop.parse::<f32>() {
            return Some(Brightness {
                max: 100,
                current: (parsed_prop * 100.0) as u32,
            });
        }
    }

    None
}
