//! Fetch information about installed CPU
//!
//! Feature `cpu` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(any(target_os = "linux", target_os = "android", target_os = "macos"))]
#![cfg(feature = "cpu")]

#[allow(unused_imports)]
use crate::{
    types,
    util::{self, log_err},
};

mod android;
mod linux;
mod macos;

use regex_lite::Regex;

/// Contains basic information about CPU
#[derive(Clone, Debug, Default, PartialEq)]
pub struct CPUInfo {
    pub vendor: String,
    pub model: String,
    pub frequency: types::Frequency,
    pub cores: u8,
    pub threads: u8,
    pub temperature: Option<f32>,
}

fn cut_model_name_trash(cpuinfo: &mut CPUInfo) {
    for re_trash in [
        "(Dual|Quad|Six|Eight)-Core",
        "[[:digit:]]? COMPUTE CORES",
        "[[:digit:]]\\w\\+[[:digit:]]\\w",
        "[[:digit:]]+-Core", // "2-Core", "4-Core", etc.
        "\\((R|r)\\)",
        "\\((TM|tm)\\)",
        "R[[:digit:]]",
    ] {
        let re = Regex::new(re_trash).unwrap();
        if let Some(m) = re.find(&cpuinfo.model) {
            cpuinfo.model = cpuinfo.model.replace(m.as_str(), "")
        }
    }
    for trash in [
        ",",
        ".",
        "AMD",
        "based",
        "CPU",
        "Graphics",
        "Intel",
        "Mobile",
        "Processor",
        "Radeon",
        "RADEON",
        "redwood",
        "Series",
        "Technologies, Inc",
        "Vega",
        "with",
    ] {
        cpuinfo.model = cpuinfo.model.replace(trash, "")
    }
}
fn extract_model_name(cpuinfo: &mut CPUInfo) {
    cpuinfo.model = util::string::remove_multiple_spaces(
        cpuinfo
            .model
            .split('@')
            .next()
            .unwrap_or(&cpuinfo.model)
            .trim(),
    );
}
fn extract_vendor_from_model(cpuinfo: &mut CPUInfo) {
    for vendor in ["Apple", "AMD", "Intel", "Qualcomm", "Unisoc"] {
        if cpuinfo.model.contains(vendor) {
            cpuinfo.model = cpuinfo.model.replace(vendor, "");
            cpuinfo.vendor = vendor.to_string();
            break;
        }
    }
}
fn beautify(cpuinfo: &mut CPUInfo) {
    cpuinfo.vendor = match cpuinfo.vendor.as_str() {
        "GenuineIntel" => "Intel",
        "AuthenticAMD" => "AMD",
        another => another,
    }
    .to_string();
    cpuinfo.model = cpuinfo.model.trim().to_string();
}

/// Returns [`std::io::Result`]<[`CPUInfo`]>
///
/// * raw_model arg - return [`CPUInfo::model`] without any processing (as is)
pub fn fetch_info(raw_model: bool) -> std::io::Result<CPUInfo> {
    let mut result = CPUInfo::default();

    #[cfg(any(target_os = "linux", target_os = "android"))]
    {
        linux::parse_proc_cpuinfo(&mut result)?;
        #[cfg(target_os = "android")]
        {
            android::fetch_cpu_model(&mut result);
            android::fetch_cpu_vendor(&mut result);
        }
        linux::fetch_threads_count(&mut result.threads);
        linux::fetch_max_freq(&mut result.frequency);
        linux::fetch_temperature(&mut result.temperature);
    }
    #[cfg(target_os = "macos")]
    {
        let (_, _, _, _) = (
            log_err("CPU/Model", macos::fetch_cpu_model(&mut result.model)),
            log_err("CPU/Cores", macos::fetch_cores_count(&mut result.cores)),
            log_err("CPU/Threads", macos::fetch_threads(&mut result.threads)),
            log_err("CPU/Frequency", macos::fetch_freq(&mut result.frequency)),
        );
    }

    // beautify CPUInfo
    if !raw_model {
        extract_model_name(&mut result);
        cut_model_name_trash(&mut result);
    }
    extract_vendor_from_model(&mut result);
    beautify(&mut result);

    // if failed to fetch cores
    if result.cores == 0 && result.threads != 0 {
        result.cores = result.threads;
    }

    Ok(result)
}
