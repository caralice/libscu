#![cfg(any(target_os = "linux", target_os = "android"))]

use crate::{
    types,
    util::{self, platform::linux::libc::get_nprocs_conf},
};

use super::CPUInfo;

use std::{
    fs::{self, OpenOptions},
    io::{BufRead, BufReader},
    path::PathBuf,
};

pub fn parse_proc_cpuinfo(cpuinfo: &mut CPUInfo) -> std::io::Result<()> {
    let file = OpenOptions::new().read(true).open("/proc/cpuinfo")?;
    let mut cpuinfo_read = BufReader::new(file);
    let mut line_buf = String::new();
    while cpuinfo_read.read_line(&mut line_buf).is_ok_and(|b| b > 0) {
        if line_buf.ends_with("\n\n") {
            break; // Exit after first block
        }

        let (var, val) = line_buf.split_once(':').unwrap_or(("", ""));
        let (var, val) = (var.trim(), val.trim().to_string());
        if var.is_empty() || val.is_empty() {
            continue;
        }
        match var {
            "model name" | "Hardware" => {
                cpuinfo.model = util::string::remove_multiple_spaces(&val);
            }
            "vendor_id" => cpuinfo.vendor = val,
            "cpu cores" => {
                if let Ok(cores) = val.trim().parse::<u8>() {
                    cpuinfo.cores = cores;
                }
            }
            "cpu MHz" => {
                let freq = util::string::extract_u64(val.split(".").next().unwrap_or("0"));
                cpuinfo.frequency = types::Frequency::from_mhz(freq as u32);
            }
            "cpu" => {
                if val.contains("POWER9") {
                    cpuinfo.model = "POWER9".to_string();
                    cpuinfo.vendor = "IBM".to_string();
                }
            }
            _ => {}
        }

        line_buf.clear();
    }
    Ok(())
}

pub fn fetch_max_freq(freq: &mut types::Frequency) {
    for file in [
        "base_frequency",
        "bios_limit",
        "scaling_max_freq",
        "cpuinfo_max_freq",
    ] {
        if let Ok(freqfile) =
            fs::read_to_string(format!("/sys/devices/system/cpu/cpu0/cpufreq/{file}"))
        {
            *freq = types::Frequency::from_hz(util::string::extract_u64(&freqfile));
            break;
        }
    }
}

pub fn fetch_threads_count(threads: &mut u8) {
    unsafe {
        let _threads = get_nprocs_conf() as u8;
        if _threads > *threads {
            *threads = _threads;
        }
    }
}

pub fn fetch_temperature(temp: &mut Option<f32>) {
    if let Ok(listdir) = util::fs::list_dir_from_str("/sys/class/hwmon", true) {
        for hwmon in listdir {
            let hwmon = PathBuf::from("/sys/class/hwmon").join(hwmon);
            if fs::metadata(hwmon.join("name")).is_err() {
                continue;
            }
            if ["k10temp", "coretemp"].contains(
                &fs::read_to_string(hwmon.join("name"))
                    .unwrap_or_default()
                    .trim(),
            ) {
                let mut hwmon_files_sorted =
                    util::fs::list_dir(hwmon.clone(), true).unwrap_or(Vec::new());
                hwmon_files_sorted.sort_by(|a, b| {
                    a.file_name()
                        .unwrap()
                        .to_ascii_lowercase()
                        .cmp(&b.file_name().unwrap().to_ascii_lowercase())
                });

                for f in hwmon_files_sorted {
                    if let Some(fname) = f.file_name().and_then(std::ffi::OsStr::to_str) {
                        if fname.starts_with("temp") && fname.ends_with("_input") {
                            if let Ok(fread) = fs::read_to_string(f) {
                                if let Ok(parsed) = fread.trim().parse::<u32>() {
                                    *temp = Some(parsed as f32 / 1000.0);
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
