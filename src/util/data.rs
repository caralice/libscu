mod des;
pub mod pci_ids;
mod pkg_managers;
mod shells;
mod wms;
mod macos_device;

#[cfg(feature = "graphics")]
pub use des::KNOWN_DESKTOP_ENVIRONMENTS;
#[cfg(any(feature = "shell", feature = "users"))]
pub use shells::KNOWN_SHELLS;
#[cfg(feature = "graphics")]
pub use wms::KNOWN_WINDOW_MANAGERS;

#[cfg(all(any(target_os = "linux", target_os = "android"), feature = "packages"))]
pub use pkg_managers::LINUX_PACKAGE_MANAGER_LIST_COMMAND;
#[cfg(all(target_os = "macos", feature = "packages"))]
pub use pkg_managers::MACOS_PACKAGE_MANAGER_LIST_COMMAND;

#[cfg(all(target_os = "macos", feature = "device"))]
pub use macos_device::get as get_macos_device_name;