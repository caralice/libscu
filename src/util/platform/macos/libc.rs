#![cfg(target_os = "macos")]
#![allow(non_camel_case_types)]

use super::unix::libc::Passwd;

use std::ffi::{c_int, c_uint};

type uint64_t = u64;

pub const CTL_KERN: c_int = 1;
pub const CTL_HW: c_int = 6;

pub const HW_MODEL: c_int = 2;
pub const HW_NCPU: c_int = 3;
pub const HW_CPU_FREQ: c_int = 15;
pub const HW_MEMSIZE: c_int = 24;

pub const KERN_VERSION: c_int = 4;
pub const KERN_HOSTNAME: c_int = 10;
pub const KERN_PROC: c_int = 14;

pub const KERN_PROC_ALL: c_int = 0;

pub const PID_MAX: usize = 30000;
pub const MAXCOMLEN: usize = 16;

extern "C" {
    pub fn getuid() -> c_uint;
    pub fn getpwuid(uid: c_int) -> *mut Passwd;

    // ll_c_code/macos/basic.c
    pub fn get_used_memory() -> uint64_t;
    pub fn get_swap_info(total: *mut uint64_t, used: *mut uint64_t) -> c_int;
    pub fn get_uptime_secs() -> uint64_t;
    // ll_c_code/macos/silicon_specific.c
    #[cfg(any(target_arch = "arm", target_arch = "aarch64"))]
    pub fn silicon_freq(mhz: &mut u32) -> *const c_char;
}

// ll_c_code/macos/proc.c
#[cfg(feature = "proc")]
extern "C" {
    pub fn get_parent_pid(pid: c_int, ppid_buf: *mut c_uint) -> c_int;
    pub fn get_command(pid: c_int, comm: *mut std::ffi::c_char) -> c_int;
    pub fn list_pids(pids: *mut c_int) -> c_int;
}

// ll_c_code/macos/user.c
#[cfg(feature = "users")]
extern "C" {
    pub fn get_pwd_shell() -> *const std::ffi::c_char;
    pub fn get_pwd_homedir() -> *const std::ffi::c_char;
}

// ll_c_code/macos/term_size.c
#[cfg(feature = "terminal")]
extern "C" {
    pub fn get_terminal_size(cols: *mut c_int, rows: *mut c_int) -> c_int;
}
