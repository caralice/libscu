use std::io::{Error, ErrorKind, Result};

fn invalid_file_err(file_path: &str) -> Error {
    Error::new(ErrorKind::InvalidData, format!("Invalid file {file_path}"))
}

const DICT_PATTERN: (&'static str, &'static str) = ("<dict>", "</dict>");
const KEY_PATTERN: (&'static str, &'static str) = ("<key>", "</key>");
const STRING_PATTERN: (&'static str, &'static str) = ("<string>", "</string>");

#[allow(dead_code)]
#[derive(Debug)]
enum EntryType {
    Key(String),
    String(String),
}

fn get_slice_between(str: &str, pattern_a: &str, pattern_b: &str) -> Option<String> {
    let start = match str.find(pattern_a) {
        Some(start_i) => start_i + pattern_a.len(),
        None => return None,
    };
    let end = match str.find(pattern_b) {
        Some(end_i) => end_i,
        None => return None,
    };

    if end > start {
        return Some(str[start..end].trim().to_string());
    }

    None
}

#[derive(Debug, Default, Clone)]
pub struct PListEntry {
    pub key: String,
    pub value: String,
}

impl PListEntry {
    pub fn clear(&mut self) {
        self.key.clear();
        self.value.clear();
    }
    pub fn set_key(&mut self, new_val: String) {
        self.key = new_val;
    }
    pub fn set_value(&mut self, new_val: String) {
        self.value = new_val;
    }
    pub fn is_key_empty(&self) -> bool {
        self.key.is_empty()
    }
}

#[derive(Debug, Default)]
pub struct PList {
    dict: Vec<PListEntry>,
}

impl PList {
    pub fn parse(file_path: &str) -> Result<Self> {
        let file = Self::check_file(file_path)?;

        let slice = match get_slice_between(&file, DICT_PATTERN.0, DICT_PATTERN.1) {
            Some(slice) => slice,
            None => return Err(invalid_file_err(file_path)),
        };

        let mut dict = Vec::<PListEntry>::new();

        let mut plist_entry = PListEntry::default();

        for line in slice.split('\n').filter(|l| !l.is_empty()) {
            if let Some(entry) = Self::parse_line(line) {
                match entry {
                    EntryType::Key(key) => {
                        plist_entry.set_key(key);
                    }
                    EntryType::String(value) => {
                        if !plist_entry.is_key_empty() {
                            plist_entry.set_value(value);
                            dict.push(plist_entry.clone());
                            plist_entry.clear();
                        }
                    }
                }
            };
        }

        Ok(Self { dict })
    }
    fn check_file(file_path: &str) -> Result<String> {
        let file = std::fs::read_to_string(file_path)?;

        if !file.contains("<?xml")
            || !file.contains(DICT_PATTERN.0)
            || !file.contains(DICT_PATTERN.1)
        {
            return Err(Error::from(ErrorKind::InvalidData));
        }

        Ok(file)
    }
    fn parse_line(line: &str) -> Option<EntryType> {
        if let Some(key) = get_slice_between(line, KEY_PATTERN.0, KEY_PATTERN.1) {
            Some(EntryType::Key(key))
        } else if let Some(string) = get_slice_between(line, STRING_PATTERN.0, STRING_PATTERN.1) {
            Some(EntryType::String(string))
        } else {
            None
        }
    }
    pub fn is_empty(&self) -> bool {
        self.dict.is_empty()
    }
    pub fn get(&self, key: &str) -> Option<String> {
        self.dict
            .iter()
            .find(|e| e.key == key)
            .map(|e| e.value.clone())
    }
}