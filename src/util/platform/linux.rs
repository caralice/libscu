#![cfg(any(target_os = "linux", target_os = "android"))]

pub mod libc;
mod ffi;

pub use ffi::*;