#![cfg(target_os = "android")]

mod ffi;
mod libc;

pub use ffi::getprop;
