#![cfg(any(target_os = "linux", target_os = "android"))]

use std::ffi::{c_char, c_int, c_long};

pub const STDOUT_FILENO: i32 = 1;
extern "C" {
    pub fn get_nprocs_conf() -> c_int;
    pub fn sysinfo(info: *mut SysInfo) -> c_int;
    pub fn gethostname(hostname_buf: *mut c_char, length: usize) -> c_int;
    pub fn uname(uts: *mut UtsName) -> c_int;
}

#[repr(C)]
pub struct UtsName {
    pub sysname: [c_char; 65],
    pub nodename: [c_char; 65],
    pub release: [c_char; 65],
    pub version: [c_char; 65],
    pub machine: [c_char; 65],
    pub domainname: [c_char; 65],
}

#[repr(C)]
#[derive(Debug, Copy, Clone)]
pub struct SysInfo {
    pub uptime: c_long,
}
