use super::libc::{
    gethostname as _gethostname, sysinfo as _sysinfo, uname as _uname, SysInfo, UtsName,
};

use std::ffi::{c_char, CStr};

pub fn gethostname() -> Option<String> {
    unsafe {
        let mut buf: [c_char; 65] = std::mem::zeroed();

        if _gethostname(&mut buf as *mut c_char, 65) == 0 {
            return Some(CStr::from_ptr(buf.as_ptr()).to_string_lossy().into_owned());
        }
    }

    None
}

pub fn uname_kernel_release() -> Option<String> {
    unsafe {
        let mut utsname: Box<UtsName> = Box::new(std::mem::zeroed());

        if _uname(utsname.as_mut()) != 0 {
            return None;
        }

        Some(
            CStr::from_ptr(utsname.release.as_ptr())
                .to_string_lossy()
                .into_owned(),
        )
    }
}

pub fn sysinfo() -> Option<SysInfo> {
    unsafe {
        let mut result: Box<SysInfo> = Box::new(std::mem::zeroed());

        if _sysinfo(result.as_mut()) != 0 {
            None
        } else {
            Some(*result)
        }
    }
}
