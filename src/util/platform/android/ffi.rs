#![cfg(target_os = "android")]

use super::libc::__system_property_get;

use std::ffi::{CStr, CString};
use std::os::raw::c_char;

fn into_string(ptr: *mut c_char) -> String {
    unsafe { CStr::from_ptr(ptr).to_string_lossy().into_owned() }
}

pub fn getprop(prop: &str) -> Option<String> {
    if !prop.is_ascii() {
        return None;
    }
    let val: *mut c_char = CString::default().into_raw();

    let code = unsafe { __system_property_get(CString::new(prop).unwrap().into_raw(), val) };

    if code != -1 {
        return Some(into_string(val));
    }

    None
}