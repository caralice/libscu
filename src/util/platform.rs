//! Platform-specific functions

pub mod android;
pub mod linux;
pub mod macos;
pub mod unix;
