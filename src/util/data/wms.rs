#![cfg(feature = "graphics")]

pub const KNOWN_WINDOW_MANAGERS: [(&str, &str); 10] = [
    ("kwin_wayland", "KWin"),
    ("kwin_x11", "KWin"),
    ("mutter-x11-frames", "Mutter"),
    ("xfwm4", "XFWM4"),
    ("dwm", "DWM"),
    ("Hyprland", "Hyprland"),
    ("i3", "i3"),
    ("sway", "Sway"),
    ("niri", "Niri"),
    ("openbox", "Openbox"),
];