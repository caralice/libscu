#![cfg(feature = "packages")]

#[cfg(any(target_os = "linux", target_os = "android"))]
pub const LINUX_PACKAGE_MANAGER_LIST_COMMAND: [(&str, &str); 5] = [
    ("dpkg", "--get-selections"),
    ("emerge", ""),
    ("flatpak", "list"),
    ("pacman", "-Qq"),
    ("rpm", "-qa"),
];

#[cfg(target_os = "macos")]
pub const MACOS_PACKAGE_MANAGER_LIST_COMMAND: [(&str, &str); 2] = [
    ("brew", ""),
    ("port", "installed")
];