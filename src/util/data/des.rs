#![cfg(feature = "graphics")]

pub const KNOWN_DESKTOP_ENVIRONMENTS: [(&str, &str); 4] = [
    ("gnome-shell", "GNOME"),
    ("plasmashell", "KDE Plasma"),
    ("xfce4-session", "XFCE4"),
    ("cinnamon-session", "Cinnamon"),
];