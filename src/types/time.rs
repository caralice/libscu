#[derive(Clone, Debug, Default)]
pub struct Date {
    pub year: u16,
    pub month: u8,
    pub day: u16,
}

impl Date {
    pub fn from_seconds(secs: u64) -> Self {
        Self::from_i64_tuple(days_to_civil(secs as i64 / 86400))
    }
    fn from_i64_tuple(t: (i64, i64, i64)) -> Self {
        Self {
            year: t.0 as u16,
            month: t.1 as u8,
            day: t.2 as u16,
        }
    }
}

#[derive(Clone, Debug, Default)]
pub struct Time {
    pub days: u64,
    pub hours: u64,
    pub minutes: u64,
    pub seconds: u64,
}

impl Time {
    pub fn from_seconds(_seconds: u64) -> Self {
        Self {
            days: _seconds / 86400,
            hours: (_seconds / 3600) % 24,
            minutes: (_seconds / 60) % 60,
            seconds: _seconds % 60,
        }
    }

    pub fn from_minutes(_minutes: u64) -> Self {
        Self {
            days: _minutes / 1440,
            hours: (_minutes / 60) % 24,
            minutes: (_minutes) % 60,
            seconds: (_minutes * 60) % 60,
        }
    }

    pub fn from_hours(_hours: u64) -> Self {
        Self {
            days: _hours / 24,
            hours: _hours % 24,
            minutes: (_hours * 60) % 60,
            seconds: (_hours * 3600) % 60,
        }
    }

    pub fn to_secs(&self) -> u64 {
        self.days + self.hours + self.minutes + self.seconds
    }
}

// Taken from http://howardhinnant.github.io/date_algorithms.html#civil_from_days
fn days_to_civil(days: i64) -> (i64, i64, i64) {
    let days = days + 719468;
    let era = if days >= 0 { days } else { days - 146096 } / 146097;
    let doe = days - era * 146097; // [0, 146096]
    let yoe = (doe - doe / 1460 + doe / 36524 - doe / 146096) / 365; // [0, 399]
    let doy = doe - (365 * yoe + yoe / 4 - yoe / 100); // [0, 365]
    let mp = (5 * doy + 2) / 153; // [0, 11]

    (
        yoe + era * 400,                       // year
        if mp < 10 { mp + 3 } else { mp - 9 }, // month
        doy - (153 * mp + 2) / 5 + 1,          // day
    )
}
