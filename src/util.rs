pub mod data;
pub mod platform;

pub mod string {
    pub fn remove_multiple_spaces(str: &str) -> String {
        str.split(' ')
            .filter(|s| !s.is_empty())
            .fold(String::new(), |a, b| a + b + " ")
            .trim()
            .to_string()
    }
    pub fn uniqalize(str: &str) -> String {
        let mut buf = Vec::<&str>::new();
        for word in str.split_whitespace() {
            if !buf.contains(&word) {
                buf.push(word);
            }
        }
        buf.iter().fold(String::new(), |a, b| a + b + " ")
    }
    pub fn extract_u64(str: &str) -> u64 {
        let mut result: u64 = 0;
        for c in str.chars() {
            if let Some(integer) = c.to_digit(10) {
                result *= 10;
                result += integer as u64;
            }
        }
        result
    }
    pub fn truncate(s: &str, max_chars: usize) -> &str {
        match s.char_indices().nth(max_chars) {
            None => s,
            Some((idx, _)) => &s[..idx],
        }
    }
}

pub fn which(name: &str) -> Option<String> {
    if let Ok(env_path) = std::env::var("PATH") {
        for path in env_path.split(':') {
            if path.is_empty() {
                continue;
            }
            if let Ok(readdir) = fs::list_dir_from_str(path, true) {
                for file in readdir {
                    if file.is_file()
                        && file
                            .file_name()
                            .unwrap_or_default()
                            .to_str()
                            .unwrap_or_default()
                            == name
                    {
                        return Some(file.as_path().to_str()?.to_string());
                    }
                }
            }
        }
    }

    None
}

pub fn log_err(category: &str, result: std::io::Result<()>) -> std::io::Result<()> {
    if let Err(err) = result.as_ref() {
        if cfg!(debug_assertions) {
            eprintln!("[{category}] fetch error: {err:?}");
        }
    }

    Ok(result?)
}

pub mod fs {
    use std::{fs, io::Result, os::unix::fs::PermissionsExt, path::PathBuf};

    pub fn list_dir(path: PathBuf, pass_dot_files: bool) -> Result<Vec<PathBuf>> {
        let mut result = Vec::<PathBuf>::new();
        for elem in fs::read_dir(path)?.into_iter() {
            if let Ok(e) = elem {
                if pass_dot_files
                    && e.file_name()
                        .into_string()
                        .is_ok_and(|n| n.starts_with("."))
                {
                    continue;
                }
                result.push(e.path());
            }
        }
        Ok(result)
    }
    pub fn list_dir_from_str(path: &str, pass_dot_files: bool) -> Result<Vec<PathBuf>> {
        list_dir(PathBuf::from(path), pass_dot_files)
    }
    pub fn scan_dir(path: PathBuf) -> Result<Vec<std::path::PathBuf>> {
        let mut result = Vec::<std::path::PathBuf>::new();
        let readdir = list_dir(path, true)?;
        for entry in readdir {
            if entry.is_dir() {
                scan_dir(entry)
                    .into_iter()
                    .for_each(|mut e| result.append(&mut e));
            } else {
                result.push(entry);
            }
        }
        Ok(result)
    }
    pub fn get_prop(path: &PathBuf, property: &str) -> Option<String> {
        Some(
            fs::read_to_string(path.join(property))
                .ok()?
                .trim()
                .to_string(),
        )
    }
    pub fn is_executable(file: PathBuf) -> bool {
        let metadata = match file.metadata() {
            Ok(metadata) => metadata,
            Err(_) => return false,
        };
        let permissions = metadata.permissions();
        metadata.is_file() && permissions.mode() & 0o111 != 0
    }
}
