//! Fetch OS uptime
//!
//! Feature `time` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(any(target_os = "android", target_os = "linux", target_os = "macos"))]
#![cfg(feature = "time")]

mod linux;
mod macos;

#[cfg(any(target_os = "android", target_os = "linux"))]
pub use linux::*;
#[cfg(target_os = "macos")]
pub use macos::fetch_uptime;
