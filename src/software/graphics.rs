//! Fetch information about desktop graphics running
//!
//! Features `graphics` must be enabled \
//! Feature `extract_version` is optional, used for fetching version
//!
//! Supported platforms: Android, Linux

#![cfg(any(target_os = "linux", target_os = "android", target_os = "macos"))]
#![cfg(feature = "graphics")]
#![allow(unreachable_code)]

mod android;
mod linux;
mod macos;

/// Contains Wayland and X11
#[cfg(target_os = "linux")]
#[derive(Clone, Debug)]
pub enum DisplayServer {
    Wayland,
    X11,
}

#[cfg(target_os = "linux")]
pub use linux::fetch_display_server;

/// Returns environment name (such as GNOME or KDE Plasma, look in sources)
pub fn fetch_environment() -> Option<String> {
    #[cfg(target_os = "android")]
    return Some(android::fetch_environment());
    #[cfg(target_os = "linux")]
    return linux::fetch_environment();
    #[cfg(target_os = "macos")]
    return Some(macos::fetch_environment());

    None
}

/// Contains basic information about window manager
#[derive(Clone, Debug)]
pub struct WindowManager {
    pub name: String,
    pub version: Option<String>,
}

/// Returns [`WindowManager`]
///
/// enable_version enables version fetching
pub fn fetch_window_manager(_enable_version: bool) -> Option<WindowManager> {
    #[cfg(target_os = "android")]
    return Some(android::fetch_window_manager());
    #[cfg(target_os = "linux")]
    return linux::fetch_window_manager(_enable_version);
    #[cfg(target_os = "macos")]
    return Some(macos::fetch_window_manager());

    None
}
