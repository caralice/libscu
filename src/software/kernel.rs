//! Fetch kernel version
//!
//! Feature `kernel` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(any(target_os = "android", target_os = "linux", target_os = "macos"))]
#![cfg(feature = "kernel")]

mod linux;
mod macos;

#[cfg(any(target_os = "linux", target_os = "android"))]
pub use linux::fetch_version;
#[cfg(target_os = "macos")]
pub use macos::fetch_version;
