//! Fetch information about terminal
//!
//! Features `terminal` and `proc` must be enabled
//!
//! Supported platforms: Android, Linux

#![cfg(any(target_os = "linux", target_os = "android", target_os = "macos"))]
#![cfg(feature = "terminal")]

mod linux;
mod macos;

use crate::types::Size2D;

use std::io::{Error, ErrorKind, Result};

/// Contans basic info about terminal
#[derive(Clone, Debug)]
pub struct TerminalInfo {
    pub name: String,
    pub version: Option<String>,
    /// Size in symbols (not pixels)
    pub size: Option<Size2D>,
}

/// Returns [`TerminalInfo`]
pub fn fetch_info(_fetch_version: bool) -> Result<TerminalInfo> {
    let mut result = TerminalInfo {
        name: String::from("Linux"),
        version: None,
        size: None,
    };

    #[cfg(any(target_os = "linux", target_os = "android"))]
    linux::fetch_info(&mut result, _fetch_version)?;
    #[cfg(target_os = "macos")]
    macos::fetch_info(&mut result)?;

    #[allow(unreachable_code)]
    {
        if result.version.is_none() && result.size.is_none() && result.name == "Linux".to_string() {
            Err(Error::new(ErrorKind::Other, "Unknown error"))
        } else {
            Ok(result)
        }
    }
}
