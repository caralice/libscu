#![cfg(target_os = "macos")]

use crate::util::platform::macos::libc;

use super::User;

fn cstring_to_string(raw_pointer: *const i8) -> Option<String> {
    unsafe {
        if raw_pointer.is_null() {
            return None;
        } else {
            std::ffi::CStr::from_ptr(raw_pointer)
                .to_str()
                .map(|s| s.to_string())
                .ok()
        }
    }
}

pub fn fetch_user(uid: i32) -> Option<User> {
    unsafe {
        let pwd = libc::getpwuid(uid);

        if pwd.is_null() {
            return None;
        }

        let username = cstring_to_string((*pwd).pw_name)?;
        let home_dir = cstring_to_string(libc::get_pwd_homedir());

        Some(User {
            name: username,
            uid: (*pwd).pw_uid as u32,
            gid: (*pwd).pw_gid as u32,
            home_dir: home_dir,
            shell: cstring_to_string(libc::get_pwd_shell()),
        })
    }
}

pub fn fetch_current() -> Option<User> {
    unsafe { fetch_user(libc::getuid() as i32) }
}

pub fn is_root() -> bool {
    unsafe { libc::getuid() == 0 }
}
