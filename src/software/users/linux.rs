#![cfg(any(target_os = "linux", target_os = "android"))]

use crate::util::data;

use super::User;

use std::ffi::CStr;
use std::fs;

mod libc {
    use std::ffi::{c_char, c_int};

    extern "C" {
        pub fn getpwuid(uid: c_int) -> *mut Passwd;
        pub fn getuid() -> c_int;
    }

    #[repr(C)]
    pub struct Passwd {
        pub pw_name: *mut c_char,
        pw_passwd: *mut c_char,
        pub pw_uid: c_int,
        pub pw_gid: c_int,
        pw_gecos: *mut c_char,
        pub pw_dir: *mut c_char,
        pub pw_shell: *mut c_char,
    }
}

pub fn fetch_user(uid: i32) -> Option<User> {
    unsafe {
        let pwd = libc::getpwuid(uid);

        if pwd.is_null() {
            return None;
        }

        let username = CStr::from_ptr((*pwd).pw_name)
            .to_string_lossy()
            .into_owned();
        let home_dir = CStr::from_ptr((*pwd).pw_dir).to_string_lossy().into_owned();
        let shell = CStr::from_ptr((*pwd).pw_shell)
            .to_string_lossy()
            .into_owned();

        Some(User {
            name: username,
            uid: (*pwd).pw_uid as u32,
            gid: (*pwd).pw_gid as u32,
            home_dir: if home_dir.is_empty() {
                None
            } else {
                Some(home_dir)
            },
            shell: if shell.is_empty() { None } else { Some(shell) },
        })
    }
}

pub fn fetch_current() -> Option<User> {
    unsafe { fetch_user(libc::getuid()) }
}

/// Returns [`Option`]<[`Vec`]<[`User`]>>
///
/// Works only on Linux
///
/// * `skip_pseudo_users` - Ignore all users whose shells are not described in /etc/shells \
/// (Currently /etc/shells is hard-coded)
///
/// May return [`None`] in those situations:
/// - /etc/passwd is corrupted or empty
/// - Failed to get [`User`] using libc
#[cfg(target_os = "linux")]
pub fn fetch_all(skip_pseudo_users: bool) -> Option<Vec<User>> {
    if let Ok(file_read) = fs::read_to_string("/etc/passwd") {
        let uids = file_read
            .split('\n')
            .map(|l| {
                fetch_user(
                    l.split(':')
                        .nth(2)
                        .unwrap_or_default()
                        .parse::<i32>()
                        .unwrap_or(-1),
                )
            })
            .filter_map(std::convert::identity)
            .filter(|user| {
                if skip_pseudo_users {
                    data::KNOWN_SHELLS.contains(&user.shell.clone().unwrap_or_default().as_str())
                } else {
                    true
                }
            })
            .collect::<Vec<User>>();

        if !uids.is_empty() {
            return Some(uids);
        }
    }
    None
}

pub fn is_root() -> bool {
    unsafe { libc::getuid() == 0 }
}
