#![cfg(target_os = "macos")]

use crate::util;

use super::PackageManager;

use std::process::Command;

/// Returns [`Vec`]<[`PackageManager`]>
pub fn fetch_all_managers() -> Vec<PackageManager> {
    let mut result: Vec<PackageManager> = Vec::new();

    for manager in util::data::MACOS_PACKAGE_MANAGER_LIST_COMMAND
        .into_iter()
        .filter(|mgr| util::which(mgr.0).is_some())
    {
        let (manager_name, manager_args) = manager;
        let mut manager_info = PackageManager {
            name: "",
            number_of_packages: 0,
        };

        if manager_name == "brew" {
            manager_info.name = manager_name;
            for pkgs_directory in ["/usr/local/Cellar", "/usr/local/Caskroom"] {
                if let Ok(pkgs) = util::fs::list_dir_from_str(pkgs_directory, true) {
                    manager_info.number_of_packages += pkgs.len() as u32;
                };
            }
        } else {
            if let Ok(command_output) = Command::new(manager_name).arg(manager_args).output() {
                manager_info.name = manager_name;
                manager_info.number_of_packages = command_output
                    .stdout
                    .iter()
                    .filter(|b| **b == b'\n')
                    .count() as u32
                    - if manager_name == "port" { 1 } else { 0 };
            }
        }

        if !manager_info.name.is_empty() {
            result.push(manager_info);
        }
    }

    result
}
