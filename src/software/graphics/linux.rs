#![cfg(target_os = "linux")]

#[cfg(feature = "extract_version")]
use crate::version::extract_version;
use crate::{
    software::proc,
    util::{
        data::{KNOWN_DESKTOP_ENVIRONMENTS, KNOWN_WINDOW_MANAGERS},
        string::truncate,
    },
};

use super::{DisplayServer, WindowManager};

/// Returns [`DisplayServer`]
///
/// Linux only
pub fn fetch_display_server() -> Option<DisplayServer> {
    Some(
        match std::env::var("XDG_SESSION_TYPE")
            .unwrap_or("".to_string())
            .as_str()
        {
            "wayland" => DisplayServer::Wayland,
            "x11" => DisplayServer::X11,
            _ => return None,
        },
    )
}

pub fn fetch_environment() -> Option<String> {
    for process in proc::list_pids().ok()? {
        for de in KNOWN_DESKTOP_ENVIRONMENTS {
            if let Ok(process) = proc::get_info(process) {
                if process.command.as_str() == truncate(de.0, 15) {
                    return Some(de.1.to_string());
                }
            }
        }
    }

    None
}

pub fn fetch_window_manager(_enable_version: bool) -> Option<WindowManager> {
    let mut result = WindowManager {
        name: String::new(),
        version: None,
    };

    for process in proc::list_pids().ok()? {
        for wm in KNOWN_WINDOW_MANAGERS {
            if let Ok(process) = proc::get_info(process) {
                if process.command.as_str() == truncate(wm.0, 15) {
                    result.name = wm.1.to_string();

                    #[cfg(feature = "extract_version")]
                    if _enable_version {
                        result.version = extract_version(match result.name.as_str() {
                            "Mutter" => "mutter",
                            "Hyprland" => "hyprctl",
                            _ => process.command.as_str(),
                        });
                    }
                    return Some(result);
                }
            }
        }
    }

    None
}
