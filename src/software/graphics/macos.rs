#![cfg(target_os = "macos")]

use super::WindowManager;

pub fn fetch_environment() -> String {
    "Aqua".to_string()
}

pub fn fetch_window_manager() -> WindowManager {
    WindowManager {
        name: "Quartz compositor".to_string(),
        version: None,
    }
}
