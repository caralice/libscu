#![cfg(target_os = "android")]

use super::WindowManager;

pub fn fetch_environment() -> String {
    "SurfaceFlinger".to_string()
}

pub fn fetch_window_manager() -> WindowManager {
    WindowManager {
        name: "Window Manager".to_string(),
        version: None,
    }
}