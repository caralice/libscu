//! Fetch information about installed package managers
//! Feature `packages` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(any(target_os = "linux", target_os = "android", target_os = "macos"))]
#![cfg(feature = "packages")]

mod linux;
mod macos;

/// Contains basic information about package manager
#[derive(Clone, Debug)]
pub struct PackageManager {
    pub name: &'static str,
    pub number_of_packages: u32,
}

#[cfg(any(target_os = "linux", target_os = "android"))]
pub use linux::fetch_all_managers;
#[cfg(target_os = "macos")]
pub use macos::fetch_all_managers;