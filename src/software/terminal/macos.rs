#![cfg(target_os = "macos")]

use crate::{types::Size2D, util::platform::macos::libc};

use super::TerminalInfo;

use std::io::{Error, ErrorKind, Result};

fn fetch_size() -> Option<Size2D> {
    unsafe {
        let (mut cols, mut rows): (i32, i32) = (0, 0);

        if libc::get_terminal_size(&mut cols, &mut rows) == 0 {
            Some(Size2D {
                width: cols as usize,
                height: rows as usize,
            })
        } else {
            None
        }
    }
}

pub fn fetch_info(result_buf: &mut TerminalInfo) -> Result<()> {
    let env_var = std::env::var("TERM_PROGRAM");

    match env_var {
        Ok(var) => {
            result_buf.name = match var.trim() {
                "iTerm.app" => "iTerm2".to_string(),
                "Terminal.app" | "Apple_Terminal" => "Apple Terminal".to_string(),
                "Hyper" => "HyperTerm".to_string(),
                "vscode" => "VSCode terminal".to_string(),
                "" => {
                    return Err(Error::new(
                        ErrorKind::Other,
                        "Environment variable $TERM_PROGRAM is empty",
                    ))
                }
                other => {
                    if other.is_empty() {
                        return Err(Error::new(
                            ErrorKind::Other,
                            "Environment variable $TERM_PROGRAM is empty",
                        ));
                    } else {
                        other.replace(".app", "")
                    }
                }
            };
        }
        Err(_) => {
            return Err(Error::new(
                ErrorKind::Other,
                "Failed to get $TERM_PROGRAM environment variable",
            ))
        }
    }

    result_buf.size = fetch_size();

    Ok(())
}
