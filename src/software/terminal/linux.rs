#![cfg(any(target_os = "linux", target_os = "android"))]

use crate::software::proc;
use crate::{
    types::Size2D,
    util::platform::unix::libc::{ioctl, WinSize, STDIN_FILENO, TIOCGWINSZ},
};

use super::TerminalInfo;

use std::path::Path;

fn bin_to_name(bin_name: String) -> String {
    String::from(match bin_name.as_str() {
        "alacritty" => "Alacritty",
        "deepin-terminal" => "Deepin Terminal",
        "foot" => "Foot",
        "gnome-terminal-" => "GNOME Terminal",
        "konsole" => "Konsole",
        "lxterminal" => "LXTerminal",
        "st" => "ST",
        "xfce4-terminal" => "XFCE4 Terminal",
        "kitty" => "Kitty",
        "crosh" => "ChromeOS developer shell",
        "code" => "VSCode Terminal",
        "codium" => "VSCodium Terminal",
        _ => "",
    })
}

fn fetch_size() -> Option<Size2D> {
    let mut nix_size = WinSize {
        ws_row: 0,
        ws_col: 0,
        ws_xpixel: 0,
        ws_ypixel: 0,
    };

    if unsafe { ioctl(STDIN_FILENO, TIOCGWINSZ, &mut nix_size) } == 0 {
        return Some(Size2D {
            width: nix_size.ws_col as usize,
            height: nix_size.ws_row as usize,
        });
    }

    None
}

pub fn fetch_info(result_buf: &mut TerminalInfo, fetch_version: bool) -> std::io::Result<()> {
    result_buf.name = "Linux".to_string();

    if Path::new("/data/data/com.termux/files/home/.termux").exists() {
        result_buf.name = "Termux".to_string();
        if fetch_version {
            result_buf.version = std::env::var("TERMUX_VERSION").ok()
        }
    } else {
        // still doesn't work from tmux
        let mut ppid = proc::get_ppid(proc::get_self_pid())?;
        while ppid != 1 {
            let proc_info = proc::get_info(ppid)?;
            if proc_info.command == "ld-linux-x86-64"
                && proc_info.cmdline.contains("cros-containers")
            {
                result_buf.name = "Crostini LXC container".to_string();
                break;
            }
            let got_name = bin_to_name(proc_info.command);
            if !got_name.is_empty() {
                result_buf.name = got_name;
                break;
            }
            ppid = proc::get_ppid(ppid)?;
        }

        if fetch_version {
            result_buf.size = fetch_size()
        }
    }

    Ok(())
}
