//! Fetch OS name
//!
//! Feature `os` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(any(target_os = "linux", target_os = "android", target_os = "macos"))]
#![cfg(feature = "os")]

/// Contains name of an operating system
#[derive(Clone, Debug, Default)]
pub struct OSRelease {
    pub pretty_name: String,
    pub name: String,
}

mod android;
mod linux;
mod macos;

/// Returns [`Option`]<[`OSRelease`]>
pub fn fetch_name() -> Option<OSRelease> {
    let mut result = OSRelease::default();

    #[cfg(target_os = "android")]
    android::fetch_os_release(&mut result);
    #[cfg(target_os = "linux")]
    linux::fetch_os_release(&mut result);
    #[cfg(target_os = "macos")]
    let _ = macos::fetch_os_release(&mut result);

    (result.name, result.pretty_name) = (
        result.name.trim().to_string(),
        result.pretty_name.trim().to_string(),
    );

    if result.name.is_empty() && result.pretty_name.is_empty() {
        return None;
    }

    if result.pretty_name.is_empty() {
        result.pretty_name = result.name.clone();
    }

    Some(result)
}
