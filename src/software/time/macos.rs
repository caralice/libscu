#![cfg(target_os = "macos")]

use crate::types::Time;
use crate::util::platform::macos::libc;

pub fn fetch_uptime() -> Option<Time> {
    unsafe {
        let uptime_secs = libc::get_uptime_secs();
        if uptime_secs != 0 {
            return Some(Time::from_seconds(uptime_secs));
        } else {
            return None;
        }
    }
}
