#![cfg(any(target_os = "linux", target_os = "android"))]
#![allow(dead_code)]

use crate::types::{Date, Time};
use crate::util::{fs, platform::linux::sysinfo};

use std::time::{SystemTime, UNIX_EPOCH};

pub fn fetch_uptime() -> Option<Time> {
    sysinfo().and_then(|si| Some(Time::from_seconds(si.uptime as u64)))
}

#[cfg(target_os = "linux")]
pub fn fetch_timezone() -> Option<String> {
    // read from /etc/timezone
    if let Some(tz) = fs::get_prop(&std::path::PathBuf::from("/etc"), "timezone") {
        if !tz.is_empty() {
            return Some(tz);
        }
    }
    // read from /etc/localtime symlink
    if std::fs::metadata("/etc/localtime").is_ok_and(|elt| elt.is_symlink()) {
        let splitted_path = std::fs::read_link("/etc/localtime")
            .ok()?
            .iter()
            .map(|oss| oss.to_string_lossy().into_owned())
            .collect::<Vec<String>>();

        if splitted_path.len() > 2 {
            return Some(format!(
                "{}/{}",
                splitted_path.get(splitted_path.len() - 1)?,
                splitted_path.last()?
            ));
        }
    }

    None
}

#[cfg(target_os = "linux")]
fn get_utc_offset() -> i64 {
    if let Ok(tz) = libtzfile::Tz::new("/etc/localtime") {
        if let Some(utoff) = tz
            .tzh_typecnt
            .iter()
            .filter(|tt| tt.tt_isdst == 1 && tt.tt_abbrind == 2)
            .next()
        {
            return utoff.tt_utoff as i64;
        }
    }

    0
}

#[cfg(target_os = "linux")]
pub fn fetch_time_now() -> Option<Time> {
    let tn = SystemTime::now().duration_since(UNIX_EPOCH).ok()?;

    Some(Time::from_seconds(tn.as_secs() + get_utc_offset() as u64))
}

#[cfg(target_os = "linux")]
pub fn fetch_current_date() -> Option<Date> {
    Some(Date::from_seconds(
        SystemTime::now().duration_since(UNIX_EPOCH).ok()?.as_secs(),
    ))
}
