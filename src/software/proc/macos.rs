#![cfg(target_os = "macos")]

use crate::util::platform::macos::libc;

use super::Process;

use std::io::{Error, ErrorKind, Result};

pub fn get_ppid(pid: u32) -> Result<u32> {
    unsafe {
        let mut ppid: u32 = 0;
        match libc::get_parent_pid(pid as i32, &mut ppid as *mut u32) {
            0 => {}
            errno => return Err(Error::from_raw_os_error(errno)),
        }

        Ok(ppid as u32)
    }
}

pub fn get_info(process: &mut Process, pid: u32) -> Result<()> {
    unsafe {
        let mut comm: [u8; libc::MAXCOMLEN + 1] = std::mem::zeroed();
        match libc::get_command(pid as i32, comm.as_mut_ptr() as *mut i8) {
            0 => {}
            errno => return Err(Error::from_raw_os_error(errno)),
        }
        match String::from_utf8(comm.to_vec()) {
            Ok(str) => {
                process.command = str.replace('\0', "");
                process.cmdline = process.clone().command;
            }
            Err(err) => return Err(Error::new(ErrorKind::Other, err.to_string())),
        }
    }

    Ok(())
}

pub fn list_pids() -> Result<Vec<u32>> {
    unsafe {
        let mut pids: [i32; libc::PID_MAX + 1] = [-1i32; libc::PID_MAX + 1];
        match libc::list_pids(&mut pids as *mut i32) {
            0 => {
                let mut pids = pids
                    .iter()
                    .filter(|pid| **pid > -1)
                    .map(|pid| *pid as u32)
                    .collect::<std::collections::HashSet<u32>>()
                    .into_iter()
                    .collect::<Vec<u32>>();
                pids.sort();
                Ok(pids)
            }
            errno => Err(Error::from_raw_os_error(errno)),
        }
    }
}
