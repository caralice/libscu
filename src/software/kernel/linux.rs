#![cfg(any(target_os = "linux", target_os = "android"))]

use crate::util::platform::linux::uname_kernel_release;

/// Returns kernel version
pub fn fetch_version() -> Option<String> {
    uname_kernel_release()
}
