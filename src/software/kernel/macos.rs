#![cfg(target_os = "macos")]

use sysctl::{Ctl, Sysctl};

use std::io::Result;

/// Returns kernel version
pub fn fetch_version() -> Option<String> {
    Some(Ctl::new("kern.osrelease").ok()?.value_string().ok()?)
}
