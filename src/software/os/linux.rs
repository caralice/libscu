#![cfg(target_os = "linux")]

use std::fs;

use super::OSRelease;

pub fn fetch_os_release(osrelease: &mut OSRelease) {
    for line in fs::read_to_string("/etc/os-release").unwrap().split('\n') {
        if line.starts_with("PRETTY_NAME=") {
            osrelease.pretty_name = line.split("NAME=").nth(1).unwrap().replace('\"', "");
        } else if line.starts_with("NAME=") {
            osrelease.name = line.split("NAME=").nth(1).unwrap().replace('\"', "");
        }
    }
}
