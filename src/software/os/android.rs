#![cfg(target_os = "android")]

use crate::util::platform::android;

use super::OSRelease;

pub fn fetch_os_release(osrelease: &mut OSRelease) {
    osrelease.name = String::from("Android");
    osrelease.pretty_name = osrelease.name.clone()
        + " "
        + &android::getprop("ro.build.version.release").unwrap_or("".to_string());
}
