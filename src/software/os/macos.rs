#![cfg(target_os = "macos")]

use crate::util::platform::macos::plist::PList;

use super::OSRelease;

fn parse_codename(plist: &PList) -> Option<String> {
    let mut version: Option<String> = None;

    for version_field in ["ProductUserVisibleVersion", "ProductVersion"] {
        if let Some(v) = plist.get(version_field) {
            if v.contains('.') {
                version = Some(v);
                break;
            }
        }
    }
    if let Some(version) = version {
        let mut version_splitted = version.split('.');
        let version = match version_splitted.next()?.parse::<u8>().ok()? {
            14 => "Sonoma",
            13 => "Ventura",
            12 => "Monterey",
            11 => "Big Sur",
            10 => match version_splitted.next()?.parse::<u8>().ok()? {
                16 => "Big Sur",
                15 => "Catalina",
                14 => "Mojave",
                13 => "High Sierra",
                12 => "Sierra",
                11 => "El Capitan",
                10 => "Yosemite",
                9 => "Mavericks",
                8 => "Mountain Lion",
                7 => "Lion",
                6 => "Snow Leopard",
                5 => "Leopard",
                4 => "Tiger",
                3 => "Panther",
                2 => "Jaguar",
                1 => "Puma",
                0 => "Cheetah",
                _ => {
                    return None;
                }
            },
            _ => {
                return None;
            }
        };
        return Some(version.to_string());
    }
    None
}

pub fn fetch_os_release(osrelease: &mut OSRelease) -> std::io::Result<()> {
    let parsed_plist = PList::parse("/System/Library/CoreServices/SystemVersion.plist")?;

    osrelease.name = "MacOS".to_string();
    osrelease.pretty_name = osrelease.name.clone();

    if let Some(version) = if let Some(version) = parsed_plist.get("ProductVersion") {
        Some(format!(" {version}"))
    } else if let Some(version) = parsed_plist.get("ProductUserVisibleVersion") {
        Some(format!(" {version}"))
    } else {
        None
    } {
        osrelease.name.push_str(&version);
    }

    if let Some(codename) = parse_codename(&parsed_plist) {
        let codename = format!(" {codename}");
        osrelease.name.push_str(&codename);
        osrelease.pretty_name.push_str(&codename);
    }

    Ok(())
}
