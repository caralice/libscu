//! Fetch hostname
//!
//! Feature `hostname` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(any(target_os = "linux", target_os = "android", target_os = "macos"))]
#![cfg(feature = "hostname")]

use crate::util::platform::linux::gethostname;

fn process(hostname: String, _raw: bool) -> String {
    hostname.replace(
        if cfg!(target_os = "macos") && _raw {
            ""
        } else {
            ".local"
        },
        "",
    )
}

/// Returns HOSTNAME
///
/// * _raw arg - (Currently for MacOS) removes '.local' from result
pub fn fetch(_raw: bool) -> Option<String> {
    Some(process(gethostname()?, _raw))
}
