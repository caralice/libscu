//! Fetch information about users
//!
//! Feature `users` must be enabled
//!
//! Supported platforms: Android, Linux, MacOS

#![cfg(any(target_os = "linux", target_os = "android", target_os = "macos"))]
#![cfg(feature = "users")]

mod linux;
mod macos;

/// Contains basic information about user
#[derive(Clone, Debug)]
pub struct User {
    pub name: String,
    pub uid: u32,
    pub gid: u32,
    pub home_dir: Option<String>,
    pub shell: Option<String>,
}

/// Returns [`Option`]<[`User`]> from known UID
///
/// May return [`None`] is those situations:
/// - UID doesn't exists
/// - libc returned a null pointer
/// - Username is a null pointer
pub fn fetch_user(uid: i32) -> Option<User> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return linux::fetch_user(uid);
    #[cfg(target_os = "macos")]
    return macos::fetch_user(uid);

    #[allow(unreachable_code)]
    None
}

/// Returns [`Option`]<[`User`]> of current user
///
/// Calls [`fetch_user`] with automatically detected user identifier
pub fn fetch_current() -> Option<User> {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return linux::fetch_current();
    #[cfg(target_os = "macos")]
    return macos::fetch_current();

    #[allow(unreachable_code)]
    None
}

#[cfg(target_os = "linux")]
pub use linux::fetch_all;

/// Check if program is running by root user
pub fn is_root() -> bool {
    #[cfg(any(target_os = "linux", target_os = "android"))]
    return linux::is_root();
    #[cfg(target_os = "macos")]
    return macos::is_root();

    #[allow(unreachable_code)]
    false
}
